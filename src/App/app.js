import React from "react";
import "./app.scss";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "../Components/Home";

const App = () => {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
